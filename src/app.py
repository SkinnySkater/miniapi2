# -*- coding: utf-8 -*-

from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask import Response
import json
import sys
from Error import *
from Config import *
from pprint import pprint
import re
import uuid

import logging
from datetime import tzinfo, timedelta, datetime

SUCCESS = "OK"

app = Flask(__name__)

conf = Config(app)
app = conf.app
conn = conf.conn

file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

def execute_query(query, args):
	cur = conn.cursor()
	cur.execute(query, args)
	res = cur.fetchall()
	cur.close()
	return res
"""
	Step 1: curl -X "GET" /api/recipes/:american-fries.json

"""

"""
	STEP 4
	curl -X "POST"
	-d "name=coca+cola&slug=cola&step[]=aller+au+supermarché"
	-H "Content-type: application/x-www-form-urlencoded"
	-H "authorization: __userpassword__"
	/api/recipes.json
"""
def auth_check(args):
	cur = conn.cursor()
	query = "SELECT `users__user`.`password` FROM `users__user` WHERE `users__user`.`password` = %s"
	cur.execute(query, args.get('Authorization'))
	res = cur.fetchall()
	cur.close()
	if (len(res) > 0):
		return True
	else:
		return False

def check_slug(args):
	cur = conn.cursor()
	query = "SELECT slug FROM recipes__recipe  WHERE slug = %s"
	slug = execute_query(query, args.get("slug"))
	cur.close()
	if (len(slug) > 0):
		return True
	else:
		return False

def post_result(user_data, req_data, step):
	req_resp = {
			'code' : 201,
			'message' : "Created",
			"datas" : {
					'id' : req_data.get('id'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : user_data.get('username'),
							'last_login' : user_data.get('last_login'),
							'id' : user_data.get('id')
					},
					'slug' : req_data.get('slug'),
					'step' : step.replace('[', '').replace(']', '').replace('\'', '').split(',')
				}
		}
	return req_resp

def add_new_reciepes(slug, name, pwd, steps):
	cur = conn.cursor()
	query_user = "SELECT id, username, last_login FROM users__user  WHERE password = %s"
	query_reci = "SELECT id, name, slug FROM recipes__recipe  WHERE slug = %s"
	query_insert = "INSERT INTO recipes__recipe VALUES (default, %s, %s, %s, %s)"
	cur.execute(query_user, pwd)
	columns = cur.description
	user_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	cur.close()
	try:
		cur = conn.cursor()
		cur.execute(query_insert, (user_info[0].get('id'), name, slug, steps))
		conn.commit()
		cur.close()
	except Exception as e:
		print(e)
		conn.rollback()
		cur.close()
		return unknown_ext(400, "Bad Request")
	cur = conn.cursor()
	cur.execute(query_reci, slug)
	columns = cur.description
	rec_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req_data = post_result(user_info[0], rec_info[0], steps)
	res = jsonify(req_data)
	res.status_code = 201
	cur.close()
	return res, 201

@app.route("/api/recipes.<json>", methods=['GET', 'POST'])
def domains(json):
	cur = conn.cursor()
	if (str(json) == "json" and request.method == "GET"):
		query = "SELECT  id, name, slug FROM recipes__recipe"
		cur.execute(query)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req_data = result
		req_resp = {}
		req_resp['code'] = 200
		req_resp['message'] = "OK"
		req_resp['datas'] = req_data
		res = jsonify(req_resp)
		res.status_code = 200
		cur.close()
		return res
	if (str(json) == "json" and request.method == "POST"):
		app.logger.info('body------> ' + str(request.data) + ' \nHeaders:-----> ' + str(request.headers) + '\n args:-----> ' + str(request.args) + '\n form:---->' + str(request.form))
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			return custom_error(400, "bad request")
		if ("Authorization" not in request.headers or not auth_check(request.headers)):
			return unknown_ext(401, "Unauthorized")
		if (not request.form.get("name") or len(request.form.get("name")) <= 0 ):
			return unknown_ext(400, "Bad Request")
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			return unknown_ext(400, "Bad Request")
		if (request.form.get("slug")):
			if (check_slug(request.form)):
				return unknown_ext(400, "Bad Request")
			else:
				slug = request.form.get("slug")
		else:
			slug = str(uuid.uuid1())
		app.logger.info(str(request.form.getlist('step[]')))
		app.logger.info(request.form.getlist('step[]'))
		return add_new_reciepes(slug, request.form.get("name"), str(request.headers.get('Authorization')), str(request.form.getlist('step[]')))
    #return post_recipes(conn, request.form, request.headers, conn.cursor(), name)
	return unknown_ext(400, "Bad Request")

"""
	Step 2: curl -X "GET" /api/recipes/:american-fries.json

"""
def slug_result(req_data):
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : {
					'id' : req_data.get('id_slug'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : req_data.get('username'),
							'last_login' : req_data.get('last_login'),
							'id' : req_data.get('id')
					},
					'slug' : req_data.get('slug')
				}
		}
	return req_resp


def check_reicipes(slug):
	cur = conn.cursor()
	query = "SELECT recipes__recipe.slug FROM recipes__recipe WHERE recipes__recipe.slug = %s"
	cur.execute(query, slug)
	cur.close()
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

@app.route("/api/<string:slug>.<ext>", methods=['GET'])
def recipes(ext, slug):
	cur = conn.cursor()
	if (str(ext) == "json" and request.method == "GET"):
		if (not check_reicipes(slug)):
				return unknown_ext(404, "Not Found")
		query = "SELECT `recipes__recipe`.`id` as id_slug, `recipes__recipe`.`name`, `recipes__recipe`.`slug`, `users__user`.`username`, `users__user`.`last_login`, `users__user`.`id`\
				FROM `recipes__recipe`\
				LEFT JOIN `users__user` ON `recipes__recipe`.`user_id` = `users__user`.`id`\
				WHERE `recipes__recipe`.`slug`=%s"
		cur.execute(query, slug)

		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req_data = slug_result(result[0])
		
		res = jsonify(req_data)
		res.status_code = 200
		cur.close()
		return res
	return unknown_ext(400, "Bad Request")


"""
	STEP 3
	curl -X "GET" /api/recipes/:american-fries/steps.json

"""
def treat_data(req_data):
	data = re.findall(r"(?<=:\").*?(?=\")", req_data, re.M|re.I)
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : data
		}
	return req_resp

@app.route("/api/recipes/<string:slug>/steps.<ext>", methods=['GET'])
def step3(ext, slug):
	cur = conn.cursor()
	if (str(ext) == "json" and request.method == "GET"):
		if (not check_reicipes(slug)):
				return unknown_ext(404, "Not Found")
		query = "SELECT `recipes__recipe`.`step`\
				FROM `recipes__recipe`\
				WHERE `recipes__recipe`.`slug`=%s"
		cur.execute(query, slug)
		columns = cur.description
		result = cur.fetchall()
		req_data = treat_data(result[0][0])
		
		res = jsonify(req_data)
		res.status_code = 200
		cur.close()
		return res
	return unknown_ext(400, "Bad Request")



@app.errorhandler(404)
def pageNotFound(error):
	code = 404
	req_resp = {}
	req_resp['code'] = code
	req_resp['message'] = "Not Found"
	res = jsonify(req_resp)
	res.status_code = code
	return res, code

if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)