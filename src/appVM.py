# -*- coding: utf-8 -*-

from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask import Response
import json
import sys
import re
from Error import *
from ConfigVM import *
from pprint import pprint
import uuid

import logging
from datetime import tzinfo, timedelta, datetime

SUCCESS = "OK"

app = Flask(__name__)

conf = Config(app)
app = conf.app
conn = conf.conn

file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)



def execute_query(query, args):
	cur = conn.cursor()
	cur.execute(query, args)
	res = cur.fetchall()
	cur.close()
	return res
"""
	Step 1: curl -X "GET" /api/recipes/:american-fries.json

"""

"""
	STEP 4
	curl -X "POST"
	-d "name=coca+cola&slug=cola&step[]=aller+au+supermarché"
	-H "Content-type: application/x-www-form-urlencoded"
	-H "authorization: __userpassword__"
	/api/recipes.json
"""
def auth_check(args):
	cur = conn.cursor()
	query = "SELECT `users__user`.`password` FROM `users__user` WHERE `users__user`.`password` = %s"
	cur.execute(query, args.get('Authorization'))
	res = cur.fetchall()
	cur.close()
	if (len(res) > 0):
		return True
	else:
		return False

def check_slug(args):
	cur = conn.cursor()
	query = "SELECT slug FROM recipes__recipe  WHERE slug = %s"
	slug = execute_query(query, args.get("slug").strip())
	cur.close()
	if (len(slug) > 0):
		return True
	else:
		return False

def isNotBlank(myString):
    return bool(myString and myString.strip())

def clear_list(lst):
	l = []
	for x in lst:
		if (isNotBlank(x)):
			l.append(x);
	return l

def post_result(user_data, req_data, step):
	req_resp = {
			'code' : 201,
			'message' : "Created",
			"datas" : {
					'id' : req_data.get('id'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : user_data.get('username'),
							'last_login' : user_data.get('last_login'),
							'id' : user_data.get('id')
					},
					'slug' : req_data.get('slug'),
					'step' : clear_list(step)
				}
		}
	return req_resp

def add_new_reciepes(slug, name, pwd, steps):
	cur = conn.cursor()
	f_steps = get_steps(steps)
	app.logger.info("step =" + str(steps))
	app.logger.info(f_steps)
	query_user = "SELECT id, username, last_login FROM users__user  WHERE password = %s"
	query_reci = "SELECT id, name, slug FROM recipes__recipe  WHERE slug = %s"
	query_insert = "INSERT INTO recipes__recipe VALUES (default, %s, %s, %s, %s)"
	cur.execute(query_user, pwd)
	columns = cur.description
	user_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	cur.close()
	try:
		cur = conn.cursor()
		app.logger.info("error step 2")
		cur.execute(query_insert, (user_info[0].get('id'), name, slug, str(steps)))
		conn.commit()
		cur.close()
	except Exception as e:
		print(e)
		conn.rollback()
		cur.close()
		app.logger.info("execute request")
		return unknown_ext(400, "Bad Request")
	cur = conn.cursor()
	cur.execute(query_reci, slug)
	columns = cur.description
	rec_info =  [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req_data = post_result(user_info[0], rec_info[0], f_steps)
	res = jsonify(req_data)
	res.status_code = 201
	cur.close()
	return res, 201

def isStep(string):
	if (string.find("step[") >= 0 and string[len(string) - 1] == ']'):
		return True
	else:
		return False

def get_steps(list_):
	l = []
	for x, y in list_.items():
		if isStep(x) and isNotBlank(y):
			l.append(y)
	app.logger.info(l)
	return l

def isStepInForn(list_):
	for x, y in list_.items():
		if isStep(x):
			return True
	return False


"""
		F I L T E R
"""

def filter(name):
	cur = conn.cursor()
	n = "%"+ name +"%"
	query = "SELECT id, name, slug FROM recipes__recipe WHERE name LIKE %s"
	cur.execute(query, n)
	columns = cur.description
	result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req_data = result
	req_resp = {}
	req_resp['code'] = 200
	req_resp['message'] = "OK"
	req_resp['datas'] = req_data
	res = jsonify(req_resp)
	res.status_code = 200
	cur.close()
	return res

@app.route("/api/recipes.<json>", methods=['GET', 'POST'])
def domains(json):
	if (str(json) == "json" and request.method == "GET"):
		if (len(request.args) == 1 and 'name' in request.args and len(request.args.get('name')) > 0):
			return filter(request.args.get('name'))
		elif (len(request.args) == 0):
			cur = conn.cursor()
			query = "SELECT  id, name, slug FROM recipes__recipe"
			cur.execute(query)
			columns = cur.description
			result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			req_data = result
			req_resp = {}
			req_resp['code'] = 200
			req_resp['message'] = "OK"
			req_resp['datas'] = req_data
			res = jsonify(req_resp)
			res.status_code = 200
			cur.close()
			return res
	if (str(json) == "json" and request.method == "POST"):
		app.logger.info('body------> ' + str(request.data) + ' \nHeaders:-----> ' + str(request.headers) + '\n args:-----> ' + str(request.args) + '\n form:---->' + str(request.form))
		app.logger.info(request.headers.get('Content-Type'))
		f_steps = get_steps(request.form)
		if ("Authorization" not in request.headers or not auth_check(request.headers)):
			return unknown_ext(401, "Unauthorized")
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			app.logger.info("error content type")
			return custom_error(400, "bad request")
		if (not request.form.get("name") or len(request.form.get("name")) <= 0):
			app.logger.info("error name")
			return unknown_ext(400, "Bad Request")
		if (len(f_steps) <= 0):
			app.logger.info("error step 2")
			return unknown_ext(400, "Bad Request")
		if (request.form.get("slug")):
			if (check_slug(request.form)):
				app.logger.info("error check slug")
				return unknown_ext(400, "Bad Request")
			else:
				slug = request.form.get("slug")
		else:
			slug = str(uuid.uuid1())
		return add_new_reciepes(slug, request.form.get("name"), str(request.headers.get('Authorization')), request.form)
	return unknown_ext(400, "Bad Request")

"""
	Step 2: curl -X "GET" /api/recipes/:american-fries.json

"""
def slug_result(req_data):
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : {
					'id' : req_data.get('id_slug'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : req_data.get('username'),
							'last_login' : req_data.get('last_login'),
							'id' : req_data.get('id')
					},
					'slug' : req_data.get('slug')
				}
		}
	return req_resp


def check_reicipes(slug):
	cur = conn.cursor()
	query = "SELECT recipes__recipe.slug FROM recipes__recipe WHERE recipes__recipe.slug = %s"
	cur.execute(query, slug)
	cur.close()
	res = cur.fetchall()
	cur.close()
	if (len(res) > 0):
		return True
	else:
		return False
"""
	STEP 5
	curl -X "PUT"
	-d "name=coca+cola+light"
	-H "Content-type: application/x-www-form-urlencoded"
	-H "authorization: __userpassword__"
	/api/recipes/:cola.json
"""

def allow_usr_put(slug, pwd):
	query = "SELECT `users__user`.`id`, `recipes__recipe`.`user_id`\
		FROM `users__user`\
		LEFT JOIN `recipes__recipe` ON `users__user`.`id` = `recipes__recipe`.`user_id`\
		WHERE `users__user`.`password` = %s AND `recipes__recipe`.`slug` = %s"
	cur = conn.cursor()
	cur.execute(query, (pwd, slug))
	#cur.close()
	res = cur.fetchall()
	cur.close()
	if (len(res) == 1):
		return True
	else:
		return False

def slug_result_put(slug):
	query = "SELECT `recipes__recipe`.`id` as id_slug, `recipes__recipe`.`name`, `recipes__recipe`.`slug`, `users__user`.`username`, `users__user`.`last_login`, `users__user`.`id`\
				FROM `recipes__recipe`\
				LEFT JOIN `users__user` ON `recipes__recipe`.`user_id` = `users__user`.`id`\
				WHERE `recipes__recipe`.`slug`=%s"
	cur = conn.cursor()
	cur.execute(query, slug)
	columns = cur.description
	result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req_data = result[0]
	app.logger.info(req_data)
	app.logger.info(slug)
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : {
					'id' : req_data.get('id_slug'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : req_data.get('username'),
							'last_login' : req_data.get('last_login'),
							'id' : req_data.get('id')
					},
					'slug' : req_data.get('slug')
				}
		}
	res = jsonify(req_resp)
	res.status_code = 200
	cur.close()
	return res

def put_name(name, slug):
	#no check to be done cause 
	cur = conn.cursor()
	query = "UPDATE recipes__recipe SET name = %s WHERE slug = %s"
	cur.execute(query, (name, slug))
	cur.close()
	app.logger.info("Done Update Name")

def put_slug(new_slug, old_slug):
	#no check to be done cause 
	cur = conn.cursor()
	query = "UPDATE recipes__recipe SET slug = %s WHERE slug = %s"
	cur.execute(query, (new_slug, old_slug))
	cur.close()
	app.logger.info("Done Update Slug")

def put_steps(steps, slug):
	#no check to be done cause 
	cur = conn.cursor()
	query = "UPDATE recipes__recipe SET step = %s WHERE slug = %s"
	cur.execute(query, (str(steps), slug))
	cur.close()
	app.logger.info("Done Update Slug")

def check_name_put(name):
	length = len(name) - 1
	if ((name[0] == "\'" or name[0] == "\"") and (name[length] == "\'" or name[length] == "\"") and not isNotBlank(name[1:length - 1])):
		return False
	if (name == "\"" or name == "\'"):
		return False
	if (name == "\"\"" or name == "\'\'"):
		return False
	if (name == "=\"" or name == "=\'\'"):
		return False
	return True

def check_arg(args):
	res = [False, False]
	if ("name" in args):
		res[0] = True
	if ("slug" in args):
		res[0] = True
	if (res[0] and (args.get("name") == '' or args.get("name") == None)):
		return False
	if (res[1] and (args.get("slug") == '' or args.get("slug") == None)):
		return False
	return True


def isInCathegory(id_):
	query = "SELECT recipe_id FROM recipes__recipe_category WHERE recipe_id = %s"
	cur = conn.cursor()
	cur.execute(query, id_)
	res = cur.fetchall()
	cur.close()
	if (len(res) == 1):
		return True
	else:
		return False

def cathegoryDelete(id_):
	query = "DELETE FROM recipes__recipe_category WHERE recipe_id = %s"
	cur = conn.cursor()
	cur.execute(query, id_)
	cur.close()

def delete_slug(slug):
	query = "SELECT recipes__recipe.id FROM recipes__recipe WHERE slug = %s"
	delete_query = "DELETE FROM recipes__recipe WHERE recipes__recipe.slug= %s"
	cur = conn.cursor()
	cur.execute(query, slug)
	columns = cur.description
	result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	cur.close()
	id_ = result[0].get("id")
	if (isInCathegory(id_)):
		cathegoryDelete(id_)
	cur = conn.cursor()
	cur.execute(delete_query, slug)
	cur.close()

	req_resp = {}
	req_resp['code'] = 200
	req_resp['message'] = "success"
	req_resp['datas'] = { 'id' : result[0].get("id") }
	res = jsonify(req_resp)
	res.status_code = 200
	return res, 200

def slug_result2(req_data):
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : {
					'id' : req_data.get('id_slug'),
					'name' : req_data.get('name'),
					'user' : {
							'username' : req_data.get('username'),
							'last_login' : req_data.get('last_login'),
							'id' : req_data.get('id'),
							'email' : req_data.get('email')
					},
					'slug' : req_data.get('slug')
				}
		}
	return req_resp

@app.route("/api/recipes/<string:slug>.<ext>", methods=['GET', 'PUT', "DELETE"])
def recipes(ext, slug):
	if (str(ext) == "json" and request.method == "GET"):
		if (not check_reicipes(slug)):
				return unknown_ext(404, "Not Found")
		if ("Authorization" in request.headers and auth_check(request.headers)):
			query = "SELECT `recipes__recipe`.`id` as id_slug, `recipes__recipe`.`name`, `recipes__recipe`.`slug`, `users__user`.`username`, `users__user`.`email`, `users__user`.`last_login`, `users__user`.`id`\
				FROM `recipes__recipe`\
				LEFT JOIN `users__user` ON `recipes__recipe`.`user_id` = `users__user`.`id`\
				WHERE `recipes__recipe`.`slug`=%s AND `users__user`.`password` = %s"
			cur = conn.cursor()
			cur.execute(query, (slug, request.headers.get("Authorization")))
			columns = cur.description
			result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
			app.logger.info(result)
			if result:
				req_data = slug_result2(result[0])
				res = jsonify(req_data)
				res.status_code = 200
				cur.close()
				return res
		query = "SELECT `recipes__recipe`.`id` as id_slug, `recipes__recipe`.`name`, `recipes__recipe`.`slug`, `users__user`.`username`, `users__user`.`last_login`, `users__user`.`id`\
				FROM `recipes__recipe`\
				LEFT JOIN `users__user` ON `recipes__recipe`.`user_id` = `users__user`.`id`\
				WHERE `recipes__recipe`.`slug`=%s"
		cur = conn.cursor()
		cur.execute(query, slug)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req_data = slug_result(result[0])
		
		res = jsonify(req_data)
		res.status_code = 200
		cur.close()
		return res
	if (str(ext) == "json" and request.method == "PUT"):
		app.logger.info('body------> ' + str(request.data) + ' \nHeaders:-----> ' + str(request.headers) + '\n args:-----> ' + str(request.args) + '\n form:---->' + str(request.form))
		app.logger.info(request.headers.get('Authorization'))
		app.logger.info(slug)
		list_arg = [False, False, False]
		new_slug = ""
		app.logger.info(slug)
		if (not check_reicipes(slug)):
			app.logger.info("slug can't be found")
			return unknown_ext(404, "Not Found")
		if ("Authorization" not in request.headers or not auth_check(request.headers)):
			return unknown_ext(401, "Unauthorized")
		if (not allow_usr_put(slug, request.headers.get('Authorization'))):
			return forbidden_action(403, "Forbidden")
		if ('application/x-www-form-urlencoded' != request.headers.get('Content-Type')):
			app.logger.info("error content type")
			return unknown_ext(400, "bad request")
		if (not check_arg(request.form)):
			app.logger.info("error args")
			return unknown_ext(400, "Bad Request")
		f_steps = get_steps(request.form)
		#handle Name
		if (request.form.get("name")):
			name = request.form.get("name").strip()
			if (name and len(name) <= 0 or name == "\'\'" or name == "\"\"" or not isNotBlank(name) or not (check_name_put(name))):
				app.logger.info("error name")
				return unknown_ext(400, "Bad Request")
			elif isNotBlank(name):
				list_arg[0] = True
			if (request.form.get("name") == '' or not isNotBlank(request.form.get("name"))):
				app.logger.info("error name")
				return unknown_ext(400, "Bad Request")
		#Handle Slug
		if (request.form.get("slug")):
			new_slug = request.form.get("slug").strip()
			app.logger.info(new_slug)
			if (new_slug and len(new_slug) <= 0 or new_slug == "''" or new_slug == "\"\"" or not isNotBlank(new_slug) or not (check_name_put(new_slug))):
				app.logger.info("error new slug")
				return unknown_ext(400, "Bad Request")
			if (new_slug and check_slug(request.form)):
				app.logger.info("error check slug, already in the database")
				return unknown_ext(400, "Bad Request")
			elif request.form.get("slug"):
				list_arg[1] = True
			if (request.form.get("slug") == '' or not isNotBlank(request.form.get("slug"))):
				app.logger.info("error slug")
				return unknown_ext(400, "Bad Request")
		#Handle Steps
		if (isStepInForn(request.form) and len(f_steps) <= 0):
			app.logger.info("error check slug, already in the database")
			return unknown_ext(400, "Bad Request")
		elif isStepInForn(request.form):
			list_arg[2] = True
		if (list_arg[0]):
			put_name(name, slug)
		if (list_arg[2]):
			put_steps(f_steps, slug)
		if (list_arg[1]):
			put_slug(request.form.get("slug"), slug)
			return slug_result_put(request.form.get("slug"))
		return slug_result_put(slug)
	if (str(ext) == "json" and request.method == "DELETE"):
		app.logger.info(slug)
		if (not check_reicipes(slug)):
			app.logger.info("slug can't be found")
			return unknown_ext(404, "Not Found")
		if ("Authorization" not in request.headers or not auth_check(request.headers)):
			return unknown_ext(401, "Unauthorized")
		if (not allow_usr_put(slug, request.headers.get('Authorization'))):
			return forbidden_action(403, "Forbidden")
		return delete_slug(slug)
	return unknown_ext(400, "Bad Request")

"""
	STEP 3
	curl -X "GET" /api/recipes/:american-fries/steps.json

"""
def treat_data(req_data):
	data = re.findall( r'(?<=:\").*?(?=\")', req_data, re.M|re.I)
	req_resp = {
			'code' : 200,
			'message' : SUCCESS,
			"datas" : data
		}
	return req_resp

@app.route("/api/recipes/<string:slug>/steps.<ext>", methods=['GET'])
def step3(ext, slug):
	cur = conn.cursor()
	if (str(ext) == "json" and request.method == "GET"):
		if (not check_reicipes(slug)):
				return unknown_ext(404, "Not Found")
		query = "SELECT `recipes__recipe`.`step`\
				FROM `recipes__recipe`\
				WHERE `recipes__recipe`.`slug`=%s"
		cur.execute(query, slug)
		columns = cur.description
		result = cur.fetchall()
		req_data = treat_data(result[0][0])
		
		res = jsonify(req_data)
		res.status_code = 200
		cur.close()
		return res
	return unknown_ext(400, "Bad Request")


@app.errorhandler(404)
def pageNotFound(error):
	code = 404
	req_resp = {}
	req_resp['code'] = code
	req_resp['message'] = "Not Found"
	res = jsonify(req_resp)
	res.status_code = code
	return res, code

if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)	