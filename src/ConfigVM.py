# -*- coding: utf-8 -*-

from flaskext.mysql import MySQL

class Config():
	def __init__(self, app):
		self.app = app
		mysql = MySQL()
		# MySQL configurations
		self.app.config['MYSQL_DATABASE_USER'] = 'root'
		self.app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
		self.app.config['MYSQL_DATABASE_DB'] = 'rest'
		self.app.config['MYSQL_DATABASE_HOST'] = 'localhost'
		mysql.init_app(self.app)
		self.conn = mysql.connect()